package com.kenyo.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.event.RowEditEvent;

import com.kenyo.model.Persona;

@Named
@ViewScoped
public class PersonaBean implements Serializable {
	
	private List<Persona> lstPersonas;
	
	@PostConstruct
	public void init() {
		this.lstPersonas = new ArrayList<>();
		lstPersonas.add(new Persona(1, "Juan"));
		lstPersonas.add(new Persona(2, "Luis"));
		lstPersonas.add(new Persona(3, "Carlos"));
	}
	
	public void onRowEdit(RowEditEvent event) {
		Persona persona = (Persona) event.getObject();
		System.out.println(persona.toString());
		for (Persona per : lstPersonas) {
			if(per.getId() == persona.getId())
				per.setNombre(persona.getNombre());
		}
		
	}

	public List<Persona> getLstPersonas() {
		return lstPersonas;
	}

	public void setLstPersonas(List<Persona> lstPersonas) {
		this.lstPersonas = lstPersonas;
	}
	
	

	
	

}
